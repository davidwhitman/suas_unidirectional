package com.example.dwhitman.suas_unidirectional

/**
 * Created by dwhitman on 1/2/2018.
 */
data class Counter(var value: Int = 0)