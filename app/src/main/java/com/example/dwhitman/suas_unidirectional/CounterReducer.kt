package com.example.dwhitman.suas_unidirectional

import zendesk.suas.Action
import zendesk.suas.Reducer

/**
 * Created by dwhitman on 1/2/2018.
 */
class CounterReducer : Reducer<Counter>() {
    override fun reduce(oldState: Counter, wildAction: Action<*>): Counter? {
        val action = wildAction as CounterAction

        return when(action) {
            is CounterAction.IncrementCounterAction -> Counter(oldState.value + action.value)
            is CounterAction.DecrementCounterAction -> Counter(oldState.value - action.value)
        }
    }

    override fun getInitialState() = Counter()
}