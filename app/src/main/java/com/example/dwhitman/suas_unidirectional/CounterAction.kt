package com.example.dwhitman.suas_unidirectional

import zendesk.suas.Action

/**
 * Created by dwhitman on 1/2/2018.
 */
sealed class CounterAction(val description: String) : Action<Counter>(description) {
    data class IncrementCounterAction(val value: Int) : CounterAction("IncrementCounterAction")
    data class DecrementCounterAction(val value: Int) : CounterAction("DecrementCounterAction")
}