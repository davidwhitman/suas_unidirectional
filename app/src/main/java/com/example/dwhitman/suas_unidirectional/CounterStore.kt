package com.example.dwhitman.suas_unidirectional

import zendesk.suas.LoggerMiddleware
import zendesk.suas.Store
import zendesk.suas.Suas

/**
 * Created by dwhitman on 1/2/2018.
 */
object CounterStore {
    val store: Store = Suas.createStore(CounterReducer())
            .withMiddleware(LoggerMiddleware()).build()
}