package com.example.dwhitman.suas_unidirectional

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Button
import android.widget.TextView

/**
 * Created by dwhitman on 1/2/2018.
 */
class HomeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.home)

        CounterStore.store.addListener(Counter::class.java) { state ->
            render(state)
        }

        findViewById<Button>(R.id.counter_plus).setOnClickListener { CounterStore.store.dispatch(CounterAction.IncrementCounterAction(1)) }
        findViewById<Button>(R.id.counter_minus).setOnClickListener { CounterStore.store.dispatch(CounterAction.DecrementCounterAction(1)) }
    }

    private fun render(state: Counter) {
        findViewById<TextView>(R.id.counter_value).text = state.value.toString()
    }
}